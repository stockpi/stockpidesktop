# StockpiDesktop
Belongs to the stockpi project (stockpilib, stockpiapp, stockpicli)

## Description
As the old raspberry pi did not allow us to use all the possibilities of current Gambas3 and (KDE/Plasma) desktop systems when we run the software on our laptop/pc, this version of the StockpiApp is intended for use on the desktop (and connect to the rasperry pi database).

## Installation
Install Linux, install Gambas3.
Use git pull to get the sourcecode (gb3 project is a directory) and open in Gambas3 IDE.
Open project properties, libraries, add stockpilib (if not done yet)

If stockpilib not available yet:
Get the stockpilib from the same stockpi repo, and open it, make executable.

Run from the Gambas3 IDE to check that everything works fine. Then choose make executable, mark options as "desktop link".
From then on you can start it from the icon on the desktop.

## Usage
Use on a laptop to keep track of your collection of raspberry pi boards, hats, and other microcontrollers, SBC's or electronics parts. (comparable to using on a pi apart from impractical camera position in lid)
Use on your pc to connect to your raspberry pi that runs the StockpiApp and the database to remotely have access to the data in the same way as on the pi itself (except making pictures).
Also see StockpiApp

## Support


## Roadmap
Might follow the development of StockpiApp, or might even pass it, and be further advanced.

## Contributing
Best contribution for the moment is to give us feedback eg by mail.

## Authors and acknowledgment
Wim.webgang

## License
GPL v3

## Project status
Just started (but will launch as split-off from dev-hq's StockpiApp 0.3.8 with all existing functionality)
